package org.fyntem.operation;

public class OperationAdd implements Operation {

    @Override
    public String calculateTwoNumbers(int numberOne, int numberTwo) {
        return String.valueOf(numberOne + numberTwo);
    }
}
