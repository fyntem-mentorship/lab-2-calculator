package org.fyntem.operation;

public class OperationSub implements Operation {

    @Override
    public String calculateTwoNumbers(int numberOne, int numberTwo) {
        // Define higher number first than subtract
        int result = 0;
        if (numberOne > numberTwo) {
            result = numberOne - numberTwo;
        } else if (numberOne < numberTwo) {
            result = numberTwo - numberOne;
        } else {
            System.out.println("Numbers are equal");
        }

        return String.valueOf(result);
    }
}
